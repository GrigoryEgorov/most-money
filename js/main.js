"use strict";

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

window.addEventListener('load', function () {
  if (!String.prototype.padStart) {
    String.prototype.padStart = function padStart(targetLength,padString) {
        targetLength = targetLength>>0; //truncate if number or convert non-number to 0;
        padString = String((typeof padString !== 'undefined' ? padString : ' '));
        if (this.length > targetLength) {
            return String(this);
        }
        else {
            targetLength = targetLength-this.length;
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength/padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0,targetLength) + String(this);
        }
    };
  } 

  function GetIEVersion() {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");
  
    // If IE, return version number.
    if (Idx > 0) 
      return parseInt(sAgent.substring(Idx+ 5, sAgent.indexOf(".", Idx)));
  
    // If IE 11 then look for Updated user agent string.
    else if (!!navigator.userAgent.match(/Trident\/7\./)) 
      return 11;
  
    else
      return 0; //It is not IE
  }

  $('.phone-mask').mask('+7 (999) 9999999'); // popup

  $('.opd--link').on('click', function () {
    $('.modal--opd').show();
  });
  $('.modal__close--opd').on('click', function () {
    $('.modal--opd').hide();
  });
  $('.modal__close--form').on('click', function () {
    $('.modal--form').hide();
  }); // opd 

  $('.opd--input').on('change', function () {
    $(this).parent().prev().prop('disabled', !$(this).prop('checked'));
  });
  $('.calc-opd').on('change', function () {
    $('button.calculator__results-btn').prop('disabled', !$(this).prop('checked'));
  }); // header 

  $('.nav__burger').on('click', function () {
    $('.burger-menu').addClass('burger-menu--active');
  });
  $('.burger-menu__cross').on('click', function () {
    $('.burger-menu').removeClass('burger-menu--active');
  });
  $('.nav__list-item').mouseenter(function () {
    var menuNum = $(this).attr('data-menu');
    $('.menu').removeClass('menu--visible');
    $('.menu-' + menuNum).addClass('menu--visible');
  });
  $('.menu').mouseleave(function () {
    $('.menu').removeClass('menu--visible');
  });
  var opened = false;
  $('.question__question').on('click', function () {
    opened = !opened;
    var answer = this.nextElementSibling;
    var sectionHeight = answer.scrollHeight;
    console.log(sectionHeight);
    var transition = 'transition: all 0.2s ease-out;';

    if (opened === true) {
      answer.classList.add('question__list--opened');
      $(this).find('.question__icon span:last-child').removeClass('question__icon-dash--vertical');
      answer.style.height = sectionHeight + 'px';

      var listener = function listener() {
        return answer.style.height = null;
      };

      answer.addEventListener('transitionend', listener);
      answer.removeEventListener('transitionend', listener);
    } else {
      answer.classList.remove('question__list--opened');
      $(this).find('.question__icon span:last-child').addClass('question__icon-dash--vertical');
      transition = '';
      requestAnimationFrame(function () {
        answer.style.height = sectionHeight + 'px';
        answer.style.transition = transition;
        requestAnimationFrame(function () {
          answer.style.height = 0 + 'px';
        });
      });
    }
  });

  if (GetIEVersion() == 0) {
    var heroSwiper = new Swiper('.header__swiper-main', {
      loop: true,
      slidesPerView: "auto",
      centeredSlides: true,
      loopedSlides: 5,
      speed: 400,
      pagination: {
        el: ".swiper-pagination-fraction",
        type: "fraction",
        clickable: true
      },
      navigation: {
        nextEl: ".swiper-button--next",
        prevEl: ".swiper-button--prev"
      }
    });
    var swiperOptionBottom = new Swiper('.header__swiper-thumbs', {
      loop: true,
      slidesPerView: 4,
      spaceBetween: 10,
      speed: 400,
      loopedSlides: 5,
      slideToClickedSlide: true
    });
  
    if (heroSwiper.controller != undefined) {
      heroSwiper.controller.control = swiperOptionBottom;
      swiperOptionBottom.controller.control = heroSwiper;
    }
  
    var swiperProducts = new Swiper('.products__swiper', {
      slidesPerView: "auto",
      spaceBetween: 25,
      freeMode: true,
      slidesOffsetAfter: 15
    });
    var swiperCasesMobile = new Swiper('.cases-swiper-mobile', {
      loop: true,
      slidesPerView: "auto",
      speed: 400,
      pagination: {
        el: ".bullets-cases",
        type: "bullets",
        clickable: true
      }
    });
    var swiperReviews = new Swiper('.reviews-section__swiper-main', {
      slidesPerView: 1,
      spaceBetween: 20,
      loop: true,
      navigation: {
        nextEl: ".swiperReviews-button--next",
        prevEl: ".swiperReviews-button--prev"
      },
      breakpoints: {
        766: {
          slidesPerView: 2,
          spaceBetween: 10
        }
      }
    });
    var swiperReviewsMobile = new Swiper('.reviews-section__swiper-tab', {
      slidesPerView: "auto",
      spaceBetween: 15,
      navigation: {
        nextEl: ".swiperReviews-button--next",
        prevEl: ".swiperReviews-button--prev"
      }
    });
    var swiperArticles = new Swiper('.articles__swiper', {
      slidesPerView: 1,
      spaceBetween: 30,
      initialSlide: 3,
      loop: true,
      navigation: {
        nextEl: ".swiperArticles-button--next",
        prevEl: ".swiperArticles-button--prev"
      },
      breakpoints: {
        1024: {
          slidesPerView: 3
        },
        766: {
          slidesPerView: 2
        }
      }
    });
    var swiperMostPremium = new Swiper('.most-premium__slider-desk', {
      slidesPerView: 1,
      spaceBetween: 40,
      loop: true,
      navigation: {
        nextEl: ".swiperPremium-button--next",
        prevEl: ".swiperPremium-button--prev"
      },
      breakpoints: {
        900: {
          slidesPerView: 2
        }
      }
    });
    var swiperMostPremiumMobile = new Swiper('.most-premium__slider-mob', {
      slidesPerView: 1,
      spaceBetween: 40,
      loop: true,
      navigation: {
        nextEl: ".swiperPremium-button--next",
        prevEl: ".swiperPremium-button--prev"
      },
      pagination: {
        el: ".bullets-moto",
        type: "bullets",
        clickable: true
      },
      breakpoints: {
        768: {
          slidesPerView: 1
        }
      }
    });
    var swiperCasesCarousel = new Swiper('.cases-carousel-swiper', {
      slidesPerView: 1,
      spaceBetween: 25,
      loop: true,
      navigation: {
        nextEl: ".cases-carousel--next",
        prevEl: ".cases-carousel--prev"
      },
      breakpoints: {
        1024: {
          slidesPerView: 3
        }
      }
    });
    var swiperProductsAll = new Swiper('.products-carousel', {
      slidesPerView: 1,
      loop: true,
      spaceBetween: 25,
      speed: 500,
      pagination: {
        el: ".swiper-pagination-products-bullets",
        type: "bullets",
        clickable: true,
        bulletClass: "swiper-pagination-products-bullet",
        bulletActiveClass: "swiper-pagination-products-bullet--active"
      }
    });
    var swiperTeam = new Swiper('.team-swiper', {
      loop: true,
      slidesPerView: 1,
      spaceBetween: 45,
      navigation: {
        nextEl: ".swiperTeam-button--next",
        prevEl: ".swiperTeam-button--prev"
      },
      breakpoints: {
        1024: {
          slidesPerView: 4
        },
        766: {
          slidesPerView: 3
        }
      }
    });
    var swiperTrust = new Swiper('.trust-swiper', {
      loop: true,
      slidesPerView: 1,
      centeredSlides: true,
      centeredSlidesBounds: true,
      navigation: {
        nextEl: ".swiperDocs-button--next",
        prevEl: ".swiperDocs-button--prev"
      },
      breakpoints: {
        1024: {
          slidesPerView: 3
        }
      }
    });
    var swiperReview = new Swiper('.review-page__big-slider', {
      spaceBetween: 10,
      loop: true,
      slidesPerView: "auto",
      centeredSlides: true,
      loopedSlides: 5,
      speed: 400
    });
    var swiperReviewGallery = new Swiper('.review-page__slider--gallery', {
      loop: true,
      slidesPerView: 6,
      spaceBetween: 10,
      speed: 400,
      loopedSlides: 5,
      slideToClickedSlide: true,
      direction: "horizontal",
      navigation: {
        nextEl: ".swiper-button--next",
        prevEl: ".swiper-button--prev"
      },
      breakpoints: {
        768: {
          direction: "vertical"
        }
      }
    });
  
    if (swiperReview.controller != undefined) {
      swiperReview.controller.control = swiperReviewGallery;
      swiperReviewGallery.controller.control = swiperReview;
    }
  
    var swiperSales = new Swiper('.swiper-sales', {
      slidesPerView: 1,
      spaceBetween: 25,
      loop: true,
      navigation: {
        nextEl: ".swiperSales-button--next",
        prevEl: ".swiperSales-button--prev"
      },
      breakpoints: {
        1024: {
          slidesPerView: 2
        }
      }
    }); 
  } else {
    $('.header__swiper-main--ie').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      appendArrows: $('.ie-slider-main-arrows'),
      prevArrow: '<div class="ie-slider-main ie-slider-main--prev"><img src="assets/icons/arrow.svg" class="arrow arrow--left" alt=""></div>',
      nextArrow: '<div class="ie-slider-main ie-slider-main--next"><img src="assets/icons/arrow.svg" class="arrow arrow--right" alt=""></div>',
      asNavFor: '.linked-slides-container--ie, .ie-slider-main-nums'
    });
    $('.linked-slides-container--ie').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      // centerMode: true,
      focusOnSelect: true,
      arrows: false,
      asNavFor: '.header__swiper-main--ie, .ie-slider-main-nums'
    });
    $('.ie-slider-main-nums').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1,
      arrows: false,
      asNavFor: '.header__swiper-main--ie, .linked-slides-container--ie'
    });
    $('.ie-reviews-slider').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      appendArrows: $('.reviews-section__arrows--ie'),
      prevArrow: '<div slot="button-slider" class="swiper-button swiperReviews-button--prev" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow arrow--left"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>',
      nextArrow: '<div slot="button-next" class="swiper-button swiperReviews-button--next reviews-section__arrows--next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>'
    });
    $('.ie-articles-slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      appendArrows: $('.articles-section__arrows--ie'),
      prevArrow: '<div slot="button-slider" class="swiper-button swiperArticles-button--prev" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow arrow--left"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>',
      nextArrow: '<div slot="button-next" class="swiper-button swiperArticles-button--next reviews-section__arrows--next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>'
    });
    $('.ie-most-premium-slider').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      appendArrows: $('.ie-most-premium-slider-arrows'),
      prevArrow: '<div slot="button-slider" class="swiper-button swiperPremium-button--prev" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow arrow--left"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>',
      nextArrow: '<div slot="button-next" class="swiper-button swiperPremium-button--next reviews-section__arrows--next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>'
    });
    $('.ie-cases-slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      appendArrows: $('.ie-cases-slider-arrows'),
      prevArrow: '<div slot="button-slider" class="swiper-button swiperPremium-button--prev" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow arrow--left"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>',
      nextArrow: '<div slot="button-next" class="swiper-button swiperPremium-button--next reviews-section__arrows--next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>'
    });
    $('.ie-zalog-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: false,
      dots: true
    });
    $('.ie-sales-slider').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      appendArrows: $('.sales__arrows'),
      prevArrow: '<div slot="button-slider" class="swiper-button swiperSales-button--prev" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow arrow--left"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>',
      nextArrow: '<div slot="button-next" class="swiper-button swiperSales-button--next reviews-section__arrows--next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>'
    });
    $('.ie-team-slider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      infinite: true,
      appendArrows: $('.reviews-section__arrows--team'),
      prevArrow: '<div slot="button-slider" class="swiper-button swiperSales-button--prev" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow arrow--left"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>',
      nextArrow: '<div slot="button-next" class="swiper-button swiperSales-button--next reviews-section__arrows--next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>'
    });
    $('.ie-trust-slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      appendArrows: $('.ie-trust-slider-arrows'),
      prevArrow: '<div slot="button-slider" class="swiper-button swiperDocs-button--prev" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow arrow--left"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>',
      nextArrow: '<div slot="button-next" class="swiper-button swiperDocs-button--next reviews-section__arrows--next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"><svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg" class="arrow"><path d="M.768 17.989c0 9.502 7.73 17.232 17.232 17.232 9.502 0 17.233-7.731 17.233-17.233 0-9.5-7.73-17.23-17.233-17.23C8.498.758.768 8.488.768 17.989zm33.09 0c0 8.742-7.114 15.856-15.858 15.857-8.744 0-15.857-7.113-15.857-15.857C2.143 9.246 9.256 2.133 18 2.133s15.858 7.112 15.858 15.855zM19.69 13.001l3.704 4.299h-12.29a.688.688 0 000 1.375h12.29l-3.704 4.3a.688.688 0 101.042.897l5.07-5.885-5.07-5.883a.687.687 0 10-1.042.897z" fill="#3B4144"></path></svg></div>'
    });
    $('.ie-review-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: false,
      dots: true
    });
  }
  

  // calculator 

  $('.form-value__tab--calculator').on('click', function () {
    $('.form-value__tab--calculator').removeClass('form-value__tab--calculator-active');
    $(this).addClass('form-value__tab--calculator-active');
  });
  var activePts;
  var url = window.location.href;
  var origin = window.location.origin;
  var page = url.replace(origin, '');

  if (page == '/zaim-pod-pts-legkovoy/' || page == '/zaim-pod-pts-gruzovoy/') {
    activePts = true;
  } else {
    activePts = false;
  }

  $('.calculator__tab--left').on('click', function () {
    activePts = false;
    $('.calculator-pts-show').removeClass('active');
    $('.calculator-auto-show').addClass('active');
    $(this).addClass('calculator__tab--active');
    $('.calculator__tab--right').removeClass('calculator__tab--active');
    $(".slider-sum-val").text('5 000 000');
    $(".slider-sum").slider("option", "min", 50000);
    $(".slider-sum").slider("option", "max", 10000000);
    $(".slider-sum").slider("value", 5000000);
    $(".slider-sum-desk").slider("option", "min", 50000);
    $(".slider-sum-desk").slider("option", "max", 10000000);
    $(".slider-sum-desk").slider("value", 5000000);
    $(".slider-days-val").text('12 дней');
    $(".slider-days").slider("option", "min", 1);
    $(".slider-days").slider("option", "max", 30);
    $(".slider-days").slider("value", 12);
    $(".slider-days-desk").slider("option", "min", 1);
    $(".slider-days-desk").slider("option", "max", 30);
    $(".slider-days-desk").slider("value", 12);
    $(".slider-days-val").text(formattedDays($(".slider-days").slider("value")));
    $('.calculator-result').text(calcSum());
  });
  $('.calculator__tab--right').on('click', function () {
    activePts = true;
    $('.calculator-auto-show').removeClass('active');
    $('.calculator-pts-show').addClass('active');
    $(this).addClass('calculator__tab--active');
    $('.calculator__tab--left').removeClass('calculator__tab--active');
    $(".slider-sum-val").text('1 500 000');
    $(".slider-sum").slider("option", "min", 100000);
    $(".slider-sum").slider("option", "max", 3000000);
    $(".slider-sum").slider("value", 1500000);
    $(".slider-sum-desk").slider("option", "min", 100000);
    $(".slider-sum-desk").slider("option", "max", 3000000);
    $(".slider-sum-desk").slider("value", 1500000);
    $(".slider-days-val").text('1 год');
    $(".slider-days").slider("option", "min", 3);
    $(".slider-days").slider("option", "max", 36);
    $(".slider-days").slider("value", 12);
    $(".slider-days-desk").slider("option", "min", 3);
    $(".slider-days-desk").slider("option", "max", 36);
    $(".slider-days-desk").slider("value", 12);
    $(".slider-days-val").text(formattedDaysPts($(".slider-days").slider("value")));
    $('.calculator-result').text(calcSum());
  });
  $(".slider-sum").slider({
    value: 5000000,
    min: 50000,
    max: 10000000,
    step: 10000,
    orientation: "horizontal",
    range: "min",
    animate: false,
    slide: function slide(event, ui) {
      $(".slider-sum-val").text(formatSum(ui.value));
      $('.calculator-result').text(calcSum());
      $('.slider-sum-desk').slider("option", "value", ui.value);
    }
  });
  $(".slider-sum-pts").slider({
    value: 1500000,
    min: 100000,
    max: 3000000,
    step: 10000,
    orientation: "horizontal",
    range: "min",
    animate: false,
    slide: function slide(event, ui) {
      $(".slider-sum-val").text(formatSum(ui.value));
      $('.calculator-result').text(calcSum('-pts'));
      $('.slider-sum-desk-pts').slider("option", "value", ui.value);
    }
  });
  $(".slider-sum-desk").slider({
    value: 5000000,
    min: 50000,
    max: 10000000,
    step: 10000,
    orientation: "horizontal",
    range: "min",
    animate: false,
    change: function change(event, ui) {
      $(".slider-sum-val").text(formatSum(ui.value));
      $('.calculator-result').text(calcSumDesk());
      $('.slider-sum').slider("option", "value", ui.value);
    }
  });
  $(".slider-sum-desk-pts").slider({
    value: 1500000,
    min: 100000,
    max: 3000000,
    step: 10000,
    orientation: "horizontal",
    range: "min",
    animate: false,
    change: function change(event, ui) {
      $(".slider-sum-val").text(formatSum(ui.value));
      $('.calculator-result').text(calcSumDesk('-pts'));
      $('.slider-sum-pts').slider("option", "value", ui.value);
    }
  });
  $(".slider-days").slider({
    value: 12,
    min: 1,
    max: 30,
    step: 1,
    orientation: "horizontal",
    range: "min",
    animate: false,
    slide: function slide(event, ui) {
      if (activePts) {
        $(".slider-days-val").text(formattedDaysPts(ui.value));
      } else {
        $(".slider-days-val").text(formattedDays(ui.value));
      }

      $('.calculator-result').text(calcSum());
      $('.slider-days-desk').slider("option", "value", ui.value);
    }
  });
  $(".slider-days-pts").slider({
    value: 12,
    min: 3,
    max: 36,
    step: 1,
    orientation: "horizontal",
    range: "min",
    animate: false,
    slide: function slide(event, ui) {
      $(".slider-days-val").text(formattedDaysPts(ui.value));
      $('.calculator-result').text(calcSum('-pts'));
      $('.slider-days-desk-pts').slider("option", "value", ui.value);
    }
  });
  $(".slider-days-desk").slider({
    value: 12,
    min: 1,
    max: 30,
    step: 1,
    orientation: "horizontal",
    range: "min",
    animate: false,
    change: function change(event, ui) {
      if (activePts) {
        $(".slider-days-val").text(formattedDaysPts(ui.value));
      } else {
        $(".slider-days-val").text(formattedDays(ui.value));
      }

      $('.calculator-result').text(calcSumDesk());
      $('.slider-days').slider("option", "value", ui.value);
    }
  });
  $(".slider-days-desk-pts").slider({
    value: 12,
    min: 3,
    max: 36,
    step: 1,
    orientation: "horizontal",
    range: "min",
    animate: false,
    change: function change(event, ui) {
      $(".slider-days-val").text(formattedDaysPts(ui.value));
      $('.calculator-result').text(calcSumDesk('-pts'));
      $('.slider-days-pts').slider("option", "value", ui.value);
    }
  });

  if (page == '/zaim-pod-vodnyi-transport/' || page == '/zaim-pod-moto/') {
    $('.slider-sum').slider("option", "value", 1000000);
    $('.slider-sum').slider("option", "max", 2000000);
    $('.slider-sum-desk').slider("option", "value", 1000000);
    $('.slider-sum-desk').slider("option", "max", 2000000);
  }

  function calcSum(str) {
    if (str == undefined) {
      str = '';
    }

    var sum = $('.slider-sum' + str).slider('value');
    var days = $('.slider-days' + str).slider('value');

    if (activePts) {
      return formatSum(Math.ceil((sum + sum * (days - 1) * 2.5 / 100) / days));
    } else {
      return formatSum(Math.ceil(sum + sum * days * 0.0833 / 100));
    }
  }

  function calcSumDesk(str) {
    if (str == undefined) {
      str = '';
    }

    var sum = $(".slider-sum-desk" + str).slider("value");
    var days = $(".slider-days-desk" + str).slider("value");

    if (activePts) {
      return formatSum(Math.ceil((sum + sum * (days - 1) * 2.5 / 100) / days));
    } else {
      return formatSum(Math.ceil(sum + sum * days * 0.0833 / 100));
    }
  }

  function formatSum(sum) {
    var sumStr = sum.toString();
    var formattedSum;

    if (sum < 10000) {
      formattedSum = sumStr.substring(0, 1) + " " + sumStr.substring(1, sumStr.length);
    } else if (sum < 100000) {
      formattedSum = sumStr.substring(0, 2) + " " + sumStr.substring(2, sumStr.length);
    } else if (sum < 1000000) {
      formattedSum = sumStr.substring(0, 3) + " " + sumStr.substring(3, sumStr.length);
    } else if (sum >= 10000000) {
      formattedSum = sumStr.substr(0, 2) + " " + sumStr.substr(2, 3) + " " + sumStr.substr(5, sumStr.length);
    } else {
      formattedSum = sumStr.substring(0, 1) + " " + sumStr.substring(1, 4) + " " + sumStr.substring(4, sumStr.length);
    }

    return formattedSum;
  }

  function formattedDays(days) {
    var formattedDays;

    if (days == 1 || days == 21) {
      formattedDays = days + " день";
    } else {
      formattedDays = days + " дней";
    }

    return formattedDays;
  }

  function formattedDaysPts(days) {
    var monthArr = [2, 3, 4];
    Math.trunc = Math.trunc || function(x) {
      if (isNaN(x)) {
        return NaN;
      }
      if (x > 0) {
        return Math.floor(x);
      }
      return Math.ceil(x);
    };
    var years = Math.trunc(days / 12);
    var month = days % 12;
    var formattedDays;

    if (years === 0) {
      if (month === 1) {
        formattedDays = "1 месяц";
      } else if (monthArr.indexOf(month) != -1) {
        formattedDays = month + " месяца";
      } else {
        formattedDays = month + " месяцев";
      }
    } else if (years === 1) {
      if (month === 0) {
        formattedDays = "1 год";
      } else if (month === 1) {
        formattedDays = "1 год 1 месяц";
      } else if (monthArr.indexOf(month) != -1) {
        formattedDays = "1 год " + month + " месяца";
      } else {
        formattedDays = "1 год " + month + " месяцев";
      }
    } else if (month === 0) {
      formattedDays = years + " года";
    } else if (month === 1) {
      formattedDays = years + " года 1 месяц";
    } else if (monthArr.indexOf(month) != -1) {
      formattedDays = years + " года " + month + " месяца";
    } else {
      formattedDays = years + " года " + month + " месяцев";
    }

    return formattedDays;
  }

  function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [day, month, year].join('.');
  }

  function addDays(days) {
    var date = new Date();
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  function addMonths(months) {
    var date = new Date();
    var d = date.getDate();
    date.setMonth(date.getMonth() + +months);

    if (date.getDate() != d) {
      date.setDate(0);
    }

    return date;
  } // form


  var formUrl = '/api/';
  var start = new Date();
  $('.request-form').on('submit', function (e) {
    e.preventDefault();
    var end = new Date();
    var sourceTime = end - start;
    var baseUrl = window.location.href;
    var utms = Object.fromEntries((baseUrl.match(/(?:\utm_).+?=[^&]*/g) || []).map(function (n) {
      return n.split('=');
    }));
    console.log(utms);
    var url = formUrl;
    var zaim_sum;

    if ($(this).find('div.zaim_sum').length != 0) {
      zaim_sum = $(this).find('.zaim_sum').slider('value');
    } else {
      zaim_sum = $(this).find('input.zaim_sum').val();
    }

    var days = $(this).find('.zaim_srok_date').slider('value').length == 0 ? undefined : $(this).find('.zaim_srok_date').slider('value');
    var date;

    if (activePts && days !== undefined) {
      date = formatDate(addMonths(days));
    } else if (days !== undefined) {
      date = formatDate(addDays(days));
    }

    var data = {
      "request": {
        contact_fname: $(this).find('.form-name').val(),
        contact_lname: $(this).find('.form-surname').val(),
        contact_phone: $(this).find('.phone-mask').val().replace(/\D/g, ""),
        contact_email: $(this).find('.form-email').val(),
        contact_company_name: $(this).find('.contact_company_name').val(),
        zaim_sum: zaim_sum,
        zaim_srok_date: date,
        zaim_srednplat: $(this).find('.calculator-result').length == 0 ? undefined : $(this).find('.calculator-result').text().trim().replace(/\D/g, ""),
        transport_type: $(this).find('.form-value__tab--calculator-active').attr('data-type'),
        transport_auto_marka: $(this).find('input.form-value-select--marka').val() || $(this).find('input.transport_auto_marka').val(),
        transport_auto_model: $(this).find('input.form-value-select--model').val(),
        transport_auto_year: $(this).find('input.form-year').val(),
        transport_auto_probeg: $(this).find('input.form-probeg').val(),
        transport_auto_vin: $(this).find('input.form-vin').val(),
        contact_out_time: $(this).find('input.contact_out_time').val(),
        contact_out_address: $(this).find('input.contact_out_address').val(),
        source_time: sourceTime,
        source_url: baseUrl,
        source_form_id: event.target.id,
        source_utm_campaign: utms.utm_campaign,
        source_utm_content: utms.utm_content,
        source_utm_medium: utms.utm_medium,
        source_utm_source: utms.utm_source,
        source_utm_term: utms.utm_term
      }
    };
    fetch(url, {
      method: 'POST',
      body: JSON.stringify(data)
    }).then(function (response) {
      console.log(response);
      $('.modal--form').css('display', 'flex');
    });
  });
  var contact_passport_file;
  var transport_auto_ptsfile;
  $('button.request-submit').on('click', function (e) {
    e.preventDefault();
    var end = new Date();
    var sourceTime = end - start;
    var baseUrl = window.location.href;
    var utms = Object.fromEntries((baseUrl.match(/(?:\utm_).+?=[^&]*/g) || []).map(function (n) {
      return n.split('=');
    }));
    console.log(utms);
    var url = formUrl;
    var srok = "".concat($('.request__list').find('.zaim_srok_date').val().substr(8, 2), ".").concat($('.request__list').find('.zaim_srok_date').val().substr(5, 2), ".").concat($('.request__list').find('.zaim_srok_date').val().substr(0, 4));
    var birthDate = "".concat($('.request__list').find('.contact_birth_day').val().substr(8, 2), ".").concat($('.request__list').find('.contact_birth_day').val().substr(5, 2), ".").concat($('.request__list').find('.contact_birth_day').val().substr(0, 4));
    var regAddress = "\u0433. ".concat($('.request__list').find('.contact_reg_city').val(), ", ").concat($('.request__list').find('.contact_reg_street').val(), ", \u0434. ").concat($('.request__list').find('.contact_reg_house').val(), ", \u043A\u0432. ").concat($('.request__list').find('.contact_reg_kv').val());
    var data = {
      "request": {
        contact_phone: $('.request__list').find('.phone-mask').val().replace(/\D/g, ""),
        contact_email: $('.request__list').find('.form-email').val(),
        contact_fio: $('.request__list').find('.contact_fio').val(),
        zaim_sum: $('.request__list').find('.zaim_sum').val().replace(/\D/g, ""),
        zaim_srok_date: srok,
        contact_passport_serial: $('.request__list').find('.contact_passport').val().substr(0, 4),
        contact_passport_number: $('.request__list').find('.contact_passport').val().substr(5, 10),
        contact_passport_kod: $('.request__list').find('.contact_passport_kod').val(),
        contact_passport_otdel: $('.request__list').find('.contact_passport_otdel').val(),
        contact_passport_file: contact_passport_file,
        contact_birth_date: birthDate,
        contact_birth_place: $('.request__list').find('.contact_birth_place').val(),
        contact_reg_city: $('.request__list').find('.contact_reg_city').val(),
        contact_reg_address: regAddress,
        transport_auto_marka: $('.request__list').find('input.form-value-select--marka').val() || $(this).find('input.transport_auto_marka').val(),
        transport_auto_model: $('.request__list').find('input.form-value-select--model').val(),
        transport_auto_year: $('.request__list').find('input.form-year').val(),
        transport_auto_probeg: $('.request__list').find('input.form-probeg').val(),
        transport_auto_vin: $('.request__list').find('input.form-vin').val(),
        transport_auto_ptsfile: transport_auto_ptsfile,
        source_time: sourceTime,
        source_url: baseUrl,
        source_form_id: event.target.id,
        source_utm_campaign: utms.utm_campaign,
        source_utm_content: utms.utm_content,
        source_utm_medium: utms.utm_medium,
        source_utm_source: utms.utm_source,
        source_utm_term: utms.utm_term
      }
    };

    if (contact_passport_file !== undefined) {
      var file1 = contact_passport_file;
      var reader = new FileReader();

      reader.onload = function (theFile) {
        return function (e) {
          var binaryData = e.target.result;
          var base64StringPassport = window.btoa(binaryData);
          data['request'].contact_passport_file = base64StringPassport;

          if (transport_auto_ptsfile !== undefined) {
            var file2 = transport_auto_ptsfile;

            var _reader = new FileReader();

            _reader.onload = function (theFile) {
              return function (e) {
                var binaryData = e.target.result;
                var base64StringPts = window.btoa(binaryData);
                data['request'].transport_auto_ptsfile = base64StringPts;
                fetch(url, {
                  method: 'POST',
                  body: JSON.stringify(data)
                }).then(function (response) {
                  console.log(response);
                  $('.modal--form').css('display', 'flex');
                });
              };
            }(file2);

            _reader.readAsBinaryString(file2);
          } else {
            fetch(url, {
              method: 'POST',
              body: JSON.stringify(data)
            }).then(function (response) {
              console.log(response);
              $('.modal--form').css('display', 'flex');
            });
          }
        };
      }(file1);

      reader.readAsBinaryString(file1);
    } else if (transport_auto_ptsfile !== undefined) {
      var file2 = transport_auto_ptsfile;

      var _reader2 = new FileReader();

      _reader2.onload = function (theFile) {
        return function (e) {
          var binaryData = e.target.result;
          var base64StringPts = window.btoa(binaryData);
          data['request'].transport_auto_ptsfile = base64StringPts;
          fetch(url, {
            method: 'POST',
            body: JSON.stringify(data)
          }).then(function (response) {
            console.log(response);
            $('.modal--form').css('display', 'flex');
          });
        };
      }(file2);

      _reader2.readAsBinaryString(file2);
    } else {
      fetch(url, {
        method: 'POST',
        body: JSON.stringify(data)
      }).then(function (response) {
        console.log(response);
        $('.modal--form').css('display', 'flex');
      });
    }
  }); // request 

  $('#file-input-passport').on('change', function (event) {
    var file = event.target.files[0];
    document.querySelector('.request-passport-name').innerText = file.name; // Only process image files.

    if (!file.type.match('image.*')) {
      alert("Можно загружать только изображения");
    }

    contact_passport_file = file;
  });
  $('#file-input-pts').on('change', function (event) {
    var file = event.target.files[0];
    document.querySelector('.request-pts-name').innerText = file.name; // Only process image files.

    if (!file.type.match('image.*')) {
      alert("Можно загружать только изображения");
    }

    transport_auto_ptsfile = file;
  });
  $('.request__list input[type="date"]').on('change', function (e) {
    e.target.style.color = 'black';
  });
  $('input.zaim_sum').on('input', function () {
    $(this).val($(this).val().replace(/,/g, '.').replace(/^([^\.]*\.)|\./g, '$1').replace(/[^0-9,.]/g, ''));
    var parts = $(this).val().toString().replace(/[^0-9.,]/g, '').split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");

    if (parts[1] != undefined) {
      parts[1] = parts[1].substr(0, 2);
    }

    $(this).val(parts.join('.'));
  });
  $('.contact_passport').mask('9999 999999');
  $('.contact_passport_kod').mask('999-999');

  function today() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    return today;
  }

  $('.request__list-content .zaim_srok_date').attr('min', today());

  function ageValidation() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear() - 18;
    today = yyyy + '-' + mm + '-' + dd;
    return today;
  }

  $('.request__list-content .contact_birth_date').attr('max', ageValidation());
  $('form.change-step').on('submit', function (e) {
    e.preventDefault(); // let step = $(this).attr('data-step');

    $(this).hide();
    $(this).next().show(); // $(`form[data-form="${step}"]`).show();
  }); // select 

  var models;
  var brands = [];

  if (GetIEVersion() == 0) {
    $.getJSON('../assets/json/auto.json', function (data) {
      models = data;
      
      var _iterator = _createForOfIteratorHelper(models),
          _step;
  
      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var item = _step.value;
  
          if (item.type == "marka") {
            brands.push(item.name);
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
  
      var _iterator2 = _createForOfIteratorHelper(brands.sort()),
          _step2;
  
      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var _item = _step2.value;
          $('.form-value-select--marka').append("<option value=\"".concat(_item, "\">").concat(_item, "</option>"));
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
  
      $('.form-value-select--marka').val('');
      $('.form-value-select--marka').on('change', function () {
        var _this = this;
  
        if ($(this).val() != null) {
          $('.form-value-select--model').remove();
  
          if (page == '/zaim-pod-legkovoy-avto/') {
            $('.form-value-model-wrap').append("<select id=\"select-model\" class=\"form-value-select form-value-select--model input-phone--form\"></select>");
          } else {
            $('.form-value-model-wrap').append("<select id=\"select-model\" class=\"form-value-select form-value-select--model\"></select>");
          }
  
          var brandId = models.find(function (item) {
            return item.name == $(_this).val();
          }).id;
  
          var _iterator3 = _createForOfIteratorHelper(models),
              _step3;
  
          try {
            for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
              var item = _step3.value;
  
              if (item.type == "model" && item.parent_id == brandId) {
                $('.form-value-select--model').append("<option value=\"".concat(item.name, "\">").concat(item.name, "</option>"));
              }
            }
          } catch (err) {
            _iterator3.e(err);
          } finally {
            _iterator3.f();
          }
  
          $('.form-value-select--model').flexselect();
          $('.form-value-select--model').val('');
          $('input.form-value-select--model').attr('placeholder', 'Модель');
        } else {
          $('input.form-value-select--model').val('');
        }
      });
      $("select.form-value-select").flexselect();
      $('input.form-value-select--marka').attr('placeholder', 'Марка');
      $('input.form-value-select--model').attr('placeholder', 'Модель');
      $(document).on('focus', 'input.form-value-select', function () {
        $(this).prev().prev().addClass('form-value-arrow--active');
      });
      $(document).on('focusout', 'input.form-value-select', function () {
        $(this).prev().prev().removeClass('form-value-arrow--active');
      });
      $(document).on('click', '#select-marka_flexselect_dropdown li', function () {
        $('.form-value-arrow--marka').removeClass('form-value-arrow--active');
      });
      $(document).on('click', '#select-model_flexselect_dropdown li', function () {
        $('.form-value-arrow--model').removeClass('form-value-arrow--active');
      });
    }); 
  } else {
    $('.form-value-select').remove();
    $('.form-value-marka-wrap').append('<input class="input-phone input-phone--left transport_auto_marka" placeholder="Марка" value="">');
    $('.form-value-model-wrap').append('<input class="input-phone input-phone--borderless transport_auto_model" placeholder="Модель" value="">')
  }

  
  
  //blog

  $('.blog__header .tag').on('click', function () {
    $('.blog__header .tag').removeClass('tag--active');
    $(this).addClass('tag--active');
  });
  var distance = 418607592;
  setInterval(function () {
    var now = new Date().getTime(); // vm.distance = vm.countDownDate - now;

    distance = distance - 1000;
    $('.sale-day').text(Math.floor(distance / (1000 * 60 * 60 * 24)));
    $('.sale-hour').text(Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60)));
    $('.sale-min').text(Math.floor(distance % (1000 * 60 * 60) / (1000 * 60)));
    $('.sale-sec').text(Math.floor(distance % (1000 * 60) / 1000));

    if (distance < 0) {//   if(distance < - 1000 * 60 * 60* 24){ 
      //   // reset timer to next year
      //   vm.countDownDate += (1000 * 60 * 60 * 24 * 1) * 365
      //   }
    }
  }, 1000);
});